#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = u'libgd.org'
SITENAME = u'GD Graphics Library'
SITEURL = ''

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Blogroll
LINKS =  (('BitBucket', 'https://bitbucket.org/libgd/gd-libgd'),
          ('Downloads', 'https://bitbucket.org/libgd/gd-libgd/downloads'),
          ('Issue Tracker', 'https://bitbucket.org/libgd/gd-libgd/issues'),
          ('Wiki', 'https://bitbucket.org/libgd/gd-libgd/wiki/Home'),
         )

# Social widget
SOCIAL = (#('You can add links in your config file', '#'),
          #('Another social link', '#'),
         )

DEFAULT_PAGINATION = 10

THEME = 'libgd'
