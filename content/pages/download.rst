Downloads
#########

:date: 2013-04-09
:slug: downloads
:author: Ondřej Surý

Stable release
==============

There is no stable release of 2.1.0 version yet.

Development version
===================

You can download current version of GD Graphics Library from the
`gd-libgd project on bitbucket.org`_.

.. _gd-libgd project on bitbucket.org: https://bitbucket.org/libgd/gd-libgd/downloads

